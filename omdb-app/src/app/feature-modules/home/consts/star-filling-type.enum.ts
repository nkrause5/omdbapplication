export enum StarFillingType {
  Full = 'Full',
  Half = 'Half',
  None = 'None'
}
