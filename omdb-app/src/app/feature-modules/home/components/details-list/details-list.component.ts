import { StarFillingType } from './../../consts/star-filling-type.enum';
import { ItemTypeSharedService } from './../../services/item-type-shared.service';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ItemDetails } from '../../models/item-details.model';
import { Type } from 'src/app/shared/shared/consts/type.enum';
import { Star } from '../../models/star.model';

@Component({
  selector: 'details-list',
  templateUrl: './details-list.component.html',
  styleUrls: ['./details-list.component.sass']
})
export class DetailsListComponent implements OnChanges {
  @Input() itemDetails: ItemDetails;
  public type: typeof Type = Type;
  public wholeRateNumber: number;
  public halfRateNUmber: number;
  public rating: Star[] = [];
  public starFillingType: typeof StarFillingType = StarFillingType;

  constructor(private itemTypeSharedService: ItemTypeSharedService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (
      this.itemDetails &&
      changes.itemDetails.previousValue !== changes.itemDetails.currentValue
    ) {
      const rateNumbers = this.itemDetails.imdbRating.split('.');
      this.wholeRateNumber = +rateNumbers[0];
      this.halfRateNUmber = +rateNumbers[1];

      this.generateRating();
    }

    if (this.itemDetails?.Type) {
      this.itemTypeSharedService.setItemType(this.itemDetails.Type);
    }
  }

  generateRating(): void {
    this.rating = [];

    if (this.wholeRateNumber) {
      for (let i = 0; i < this.wholeRateNumber; i++) {
        this.rating.push({ fillingType: this.starFillingType.Full });
      }
    }

    if (this.halfRateNUmber) {
        this.rating.push({ fillingType: this.starFillingType.Half });
    }
  }
}
