import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Item } from '../../models/item.model';

@Component({
  selector: 'result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.sass']
})
export class ResultListComponent {
  @Input() resultList: Item[];
  @Output() resultClick: EventEmitter<Item> = new EventEmitter<Item>();

  onResultClick(result: Item): void {
    this.resultClick.emit(result);
  }
}
