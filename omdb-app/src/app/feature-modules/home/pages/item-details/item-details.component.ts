import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { ItemDetails } from '../../models/item-details.model';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.sass']
})
export class ItemDetailsComponent implements OnInit {
  public itemDetails: ItemDetails;

  constructor(
    private searchService: SearchService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.serchForItemDetails();
  }

  serchForItemDetails(): void {
    this.route.params
      .pipe(concatMap(p => this.searchService.searchForItemDetails(p.id)))
      .subscribe(result => {
        this.itemDetails = result;
      });
  }
}
