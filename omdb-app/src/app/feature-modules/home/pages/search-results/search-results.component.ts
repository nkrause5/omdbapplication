import { ResultSharedService } from './../../services/result-shared.service';
import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/item.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.sass']
})
export class SearchResultsComponent implements OnInit {
  public itemList: Item[];

  constructor(
    private resultSharedService: ResultSharedService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(result => {
      this.itemList = this.resultSharedService.getResulList();
    });
  }

  onResultClick(result: Item): void {
    this.router.navigateByUrl('/home/details/' + result.imdbID);
  }
}
