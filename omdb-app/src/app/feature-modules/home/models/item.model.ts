import { Type } from 'src/app/shared/shared/consts/type.enum';

export interface Item {
  imdbID: string;
  Title: string;
  Type: Type;
  Poster: string;
}
