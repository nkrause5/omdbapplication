import { Item } from './item.model';

export interface SearchParams {
  items: Item[];
  keyword: string;
}
