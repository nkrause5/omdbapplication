import { StarFillingType } from '../consts/star-filling-type.enum';

export interface Star {
  fillingType: StarFillingType;
}
