import { Item } from './item.model';

export interface SearchResponse {
  Search: Item[];
}
