export interface ItemDetails {
  Poster: string;
  Title: string;
  Year: string;
  Runtime: string;
  imdbVotes: number;
  imdbRating: string;
  Director: string;
  Genre: string;
  Released: Date;
  Plot: string;
  Type: string;
  totalSeasons: number;
}
