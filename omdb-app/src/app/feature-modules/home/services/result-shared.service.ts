import { Item } from '../models/item.model';

export class ResultSharedService {
  setResultList(items: Item[]): void {
    localStorage.setItem('resultList', JSON.stringify(items));
  }

  getResulList(): Item[] {
    const resultList = localStorage.getItem('resultList');
    return JSON.parse(resultList);
  }
}
