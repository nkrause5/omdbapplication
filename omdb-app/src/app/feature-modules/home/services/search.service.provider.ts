import { SearchService } from './search.service';
import { HttpClient } from '@angular/common/http';

const searchServiceFactory = (httpClient: HttpClient) => {
  return new SearchService(httpClient);
};

export let searchServiceProvider = {
  provide: SearchService,
  useFactory: searchServiceFactory,
  deps: [HttpClient]
};
