import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemTypeSharedService {
  private itemType: Subject<string> = new Subject<string>();

  setItemType(type: string): void {
    this.itemType.next(type);
  }
  getItemType(): Observable<string> {
    return this.itemType;
  }

}
