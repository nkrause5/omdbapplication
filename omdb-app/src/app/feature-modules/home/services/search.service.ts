import { SearchResponse } from './../models/search-response.model';
import { environment } from './../../../../environments/environment.prod';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Item } from '../models/item.model';
import { ItemDetails } from '../models/item-details.model';
import { Type } from 'src/app/shared/shared/consts/type.enum';


export class SearchService {
  type: typeof Type = Type;

  private baseUrl: string = environment.URL;
  params: HttpParams = new HttpParams().set('apikey', environment.apikey);

  constructor(private http: HttpClient) {}

  searchForKeyword(keyword: any): Observable<Item[]> {
    return this.http
      .get<SearchResponse>(this.baseUrl, {
        params: this.params.set('s', keyword)
      })
      .pipe(
        map(sr => {
          if (sr) {
            return sr.Search?.filter(i => i.Type !== this.type.Game);
          } else {
            return null;
          }
        })
      );
  }

  searchForItemDetails(id: string): Observable<ItemDetails> {
    return this.http.get<ItemDetails>(this.baseUrl, {
      params: this.params.set('i', id)
    });
  }
}
