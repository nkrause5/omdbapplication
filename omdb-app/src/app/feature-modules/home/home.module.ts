import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { searchServiceProvider } from './services/search.service.provider';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { DetailsListComponent } from './components/details-list/details-list.component';
import { ResultSharedService } from './services/result-shared.service';
import { SearchResultsComponent } from './pages/search-results/search-results.component';
import { ResultListComponent } from './components/result-list/result-list.component';
import { ItemTypeSharedService } from './services/item-type-shared.service';

@NgModule({
  declarations: [SearchResultsComponent, ItemDetailsComponent, DetailsListComponent, ResultListComponent],
  imports: [CommonModule, SharedModule, HomeRoutingModule],
  providers: [searchServiceProvider, ResultSharedService]
})
export class HomeModule {}
