import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from 'src/app/shared/shared/components/main-page/main-page.component';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { SearchResultsComponent } from './pages/search-results/search-results.component';

const routes: Routes = [
  {
    path: 'home',
    component: MainPageComponent,
    children: [
      {
        path: 'results' + '/:keyword',
        component: SearchResultsComponent
      },
      {
        path: 'details' + '/:id',
        component: ItemDetailsComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
