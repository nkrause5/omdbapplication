import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MainPageComponent } from './components/main-page/main-page.component';
import { LogoComponent } from './components/logo/logo.component';
import { SearchComponent } from './components/search/search.component';
import { RouterModule } from '@angular/router';

const MATERIAL_MODULES = [
  MatAutocompleteModule,
  MatInputModule,
  MatFormFieldModule,
  BrowserAnimationsModule,
  MatButtonModule
];

@NgModule({
  declarations: [MainPageComponent, LogoComponent, SearchComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MATERIAL_MODULES,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    FlexLayoutModule,
    MATERIAL_MODULES,
    ReactiveFormsModule,
    MainPageComponent,
    LogoComponent,
    SearchComponent,
    RouterModule
  ]
})
export class SharedModule {}
