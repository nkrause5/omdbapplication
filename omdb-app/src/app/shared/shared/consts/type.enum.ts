export enum Type {
  Movie = 'movie',
  Series = 'series',
  Episode = 'episode',
  Game = 'game'
}
