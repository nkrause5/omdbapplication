import { SearchParams } from './../../../../feature-modules/home/models/search-params.model';
import { ResultSharedService } from './../../../../feature-modules/home/services/result-shared.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from 'src/app/feature-modules/home/models/item.model';

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})
export class MainPageComponent {

  constructor(private router: Router, private resultSharedService: ResultSharedService) {}

  public onOptionClick(searchField: Item): void {
    this.router.navigateByUrl('/home/details/' + searchField.imdbID);
  }

  public onSearchClick(searchParams: SearchParams): void {
    this.resultSharedService.setResultList(searchParams.items);
    this.router.navigateByUrl('/home/results/' + searchParams.keyword);
  }

}
