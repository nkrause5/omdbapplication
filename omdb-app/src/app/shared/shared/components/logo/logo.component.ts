import { ItemTypeSharedService } from './../../../../feature-modules/home/services/item-type-shared.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Type } from '../../consts/type.enum';

@Component({
  selector: 'logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.sass']
})
export class LogoComponent implements OnInit {
  public itemType: string;
  type: typeof Type = Type;

  constructor(private itemTypeSharedService: ItemTypeSharedService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.itemTypeSharedService.getItemType().subscribe(
      result => {
        this.itemType = result;
        this.changeDetector.detectChanges();
      }
    );
  }

}
