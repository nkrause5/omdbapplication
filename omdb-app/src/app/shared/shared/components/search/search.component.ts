import { SearchParams } from './../../../../feature-modules/home/models/search-params.model';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { startWith, debounceTime, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/feature-modules/home/services/search.service';
import { Item } from 'src/app/feature-modules/home/models/item.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent implements OnInit {
  @Output() optionClick: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() searchClick: EventEmitter<SearchParams> = new EventEmitter<SearchParams>();

  public searchField: FormControl = new FormControl('');
  public items: Item[];
  private keyword: string;

  constructor(private searchService: SearchService, private router: Router) {}

  ngOnInit(): void {
    this.searchForOptions();
  }

  private searchForOptions(): void {
    this.searchField.valueChanges
      .pipe(
        startWith(''),
        debounceTime(300),
        switchMap(keyword => {
          this.keyword = keyword;
          return this.searchService.searchForKeyword(keyword);
        })
      )
      .subscribe(i => {
        this.items = i;
      });
  }

  public onOptionClick(): void {
    this.optionClick.emit(this.searchField.value);
  }

  public onSearchClick(): void {
    this.searchClick.emit({items: this.items, keyword: this.keyword});
  }

  public displayWith(item: Item) {
    return item.Title;
  }
}
